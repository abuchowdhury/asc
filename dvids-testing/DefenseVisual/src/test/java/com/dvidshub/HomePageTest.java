package com.dvidshub;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.openqa.selenium.TakesScreenshot;

public class HomePageTest extends Base {

	
	
	
	@Test(priority = 0)
	public void logInSuccess() throws IOException {
		driver = initializeDriver();
		/*
		File src1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src1, new File("screenshots//screenshot1.png"));
		*/
	}

	

	
	@Test(priority = 1)
	public void userLoginValidation() {
		String actualUserName = driver.findElement(By.id("logged_in_user")).getText();
		String expectedUsername = driver.findElement(By.id("logged_in_user")).getText();
		Assert.assertEquals(actualUserName, expectedUsername);
		System.out.println("Valid user logged in successfully");
	}

	@Test(priority = 2)
	public void myAlbumCheck() {
		Actions action = new Actions(driver);
		driver.findElement(By.id("my_downloads_link")).click();
		driver.findElement(By.xpath("//*[@id=\"dvids_top_bar_right_2\"]/a")).click();
		action.moveToElement(driver.findElement(By.id("menuLinkcontent"))).build().perform();
		driver.findElement(By.xpath("//*[@id=\"dvids_main_nav\"]/li[3]/ul/li[1]/a")).click();
		//driver.findElement(By.xpath("//*[@id=\"dvids_main_nav\"]/li[3]/ul/li[2]/a")).click();
		
		System.out.println("My Album checked successfully");

	}

	
	
	@Test(priority = 3)
	public void navBarValidation() throws InterruptedException {
		driver.findElement(By.id("menuLinkhome")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("menuLinkfeatures")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("menuLinkstories")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("menuLinkunits")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("menuLinknewswire")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("menuLinkmedia")).click();
		Thread.sleep(1000);

		System.out.println("NavBar Test passed successfully");

	}
	
	@Test(priority = 4)
	public void scrollToFooter() throws IOException {
		

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
	}
	
	@Test(priority = 5)
	public void footerContentValidation() throws InterruptedException, IOException {
		scrollToFooter(); 
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[1]/a")).click();
	
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[2]/a")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[3]/a")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[4]/a")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[5]/a")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[6]/a")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[7]/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[8]/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[9]/a")).click();
		
	}

	@Test(priority = 6)
	public void footerFeatureCheck() throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[4]/h4/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[4]/ul/li[1]/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[4]/ul/li[1]/a")).click();
		
	}
	
	@Test(priority = 7)
	public void footerAboutDvidsCheck() throws InterruptedException {
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[2]/h4[2]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[2]/ul[1]/li[1]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[2]/ul[1]/li[2]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[2]/ul[1]/li[3]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[2]/ul[1]/li[4]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[2]/ul[1]/li[5]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[2]/ul[1]/li[6]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[2]/ul[1]/li[7]/a")).click();
		Thread.sleep(2000);
		
		
	}
	
	@Test(priority = 8)
	public void footerMediaRequestCheck() throws InterruptedException {
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[2]/h4[3]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[2]/ul[2]/li[1]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[2]/ul[2]/li[2]/a")).click();
		Thread.sleep(2000);
	}
	
	@Test(priority = 9)
	public void footerStoriesCheck() throws InterruptedException {
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[2]/h4[4]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[2]/ul[3]/li[1]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[2]/ul[3]/li[2]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[2]/ul[3]/li[3]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[2]/ul[3]/li[4]/a")).click();
	}
	
	@Test(priority = 10)
	
	public void footerWebSupportCheck() throws InterruptedException {
		Thread.sleep(2000);
		String actual = driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[1]/ul[2]/li/a")).getText();
		String expected = "dvidsservicedesk@defense.gov"; 
		Assert.assertEquals(actual, expected);
	}
	
	
	
//	@AfterSuite
//	public void xlogOutValidation() throws InterruptedException {
//		
//		driver.findElement(By.id("dvids_top_bar_right_4")).click();
//		System.out.println("Successfully Logged out");
//		Thread.sleep(2000); 
//		//driver.quit();
//		
//	}
//	
	/*
	 * @Test public void imagePageCheck() { Actions action = new Actions(driver);
	 * action.moveToElement(driver.findElement(By.id("menuLinkcontent"))).build().
	 * perform();
	 * driver.findElement(By.xpath("//*[@id=\"dvids_main_nav\"]/li[3]/ul/li[2]/a")).
	 * click();
	 * 
	 * }
	 */

}
