package com.dvidshub;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class BaseDocker {
	
	

	public WebDriver driver; 
	public Properties prop = new Properties(); 

	public WebDriver initializeDriver() throws IOException {

		FileInputStream fis = new FileInputStream("C:\\data.properties"); 
		prop.load(fis);
		String browserName =  prop.getProperty("browser"); 
		
		
		
		DesiredCapabilities dCapabilities = new DesiredCapabilities(); 
		
		WebDriver driver = new RemoteWebDriver(new URL("http://localhost:4444"), dCapabilities); 
		
		driver.get("https://www.dvidshub.net/");
		driver.manage().window().maximize();

		//login
		driver.findElement(By.cssSelector("a[href*='login']")).click();
		driver.findElement(By.cssSelector("[id='email']")).sendKeys(prop.getProperty("username"));
		driver.findElement(By.cssSelector("[id = 'password']")).sendKeys(prop.getProperty("password"));
		driver.findElement(By.cssSelector("[id = 'submit']")).click();
		
		return driver; 
	}
	
}
