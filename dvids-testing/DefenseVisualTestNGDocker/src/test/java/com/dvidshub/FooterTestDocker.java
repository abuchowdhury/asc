package com.dvidshub;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class FooterTestDocker extends BaseDocker{

	
	@Test(priority = 0)
	public void scrollToFooter() throws IOException {
		

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
	}
	
	@Test(priority = 1)
	public void footerContentValidation() throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[1]/a")).click();
	
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[2]/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[3]/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[4]/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[5]/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[6]/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[7]/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[8]/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"dvids_footer_container\"]/div/div[1]/div[3]/ul/li[9]/a")).click();
		
	}

	

}
