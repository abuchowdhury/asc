
package com.dvidshub;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class ListViewBrokenImageLinkDocker extends BaseDocker {

	// private static WebDriver driver = null;

	@Test(priority = 0)
	public void initializeConnection() throws IOException {

		driver = initializeDriver();
		
		
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(By.id("menuLinkcontent"))).build().perform();
		driver.findElement(By.xpath("//*[@id=\"dvids_main_nav\"]/li[3]/ul/li[1]/a")).click();

		driver.findElement(By.xpath("//*[@id=\"body_content\"]/div/div/div/nav/ul/li[1]/a")).click();

	}

	
// invocationCount is the number of pages that we want results
	@Test(invocationCount = 4)
	public void linkChecker() {

		// String totalPageNum =
		// driver.findElement(By.xpath("//*[@id=\"body_content\"]/div/div/div/ul/li[8]/a")).getText();

		
		String url = "";
		HttpURLConnection huc = null;
		int respCode = 200;

		List<WebElement> links = driver.findElements(By.tagName("img"));

		Iterator<WebElement> it = links.iterator();

		while (it.hasNext()) {

			url = it.next().getAttribute("src");

			//System.out.println(url);

			if (url == null || url.isEmpty()) {
				System.out.println("URL is either not configured for anchor tag or it is empty");
				System.out.println(url); 
				continue;
			}

			try {
				huc = (HttpURLConnection) (new URL(url).openConnection());

				huc.setRequestMethod("HEAD");

				huc.connect();

				respCode = huc.getResponseCode();

				if (respCode >= 400) {
					System.out.println(url + " image link is broken");
				} 
				/*
				else {
					System.out.println(url + " is a valid link");
				}
*/
			} catch (MalformedURLException e) {
// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		driver.findElement(By.cssSelector("a[class=\'pager_next']")).click();

	}
	
	
	
}
