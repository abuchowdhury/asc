package com.dvidshub;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType; 
//import com.mongodb.MapReduceCommand.OutputType;

public class BaseForAdmin {

	public WebDriver driver; 
	public Properties prop = new Properties(); 
	
	public WebDriver initializeDriver() throws IOException {
		 
		
		FileInputStream fis = new FileInputStream("C:\\Users\\shuvo\\eclipse-workspace\\DVIDSTicket\\src\\main\\java\\resources\\data.properties"); 
		prop.load(fis);
		String browserName =  prop.getProperty("browser"); 
		
		
		if(browserName.equals("chrome")) {
			
			System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); 
			 driver = new ChromeDriver(); 
		}
		else if(browserName.equals("firefox")) {
			
			System.setProperty("webdriver.gecko.driver","C:\\geckodriver.exe");
			 driver = new FirefoxDriver(); 
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
		
		driver.manage().window().maximize();
		//navigate to desired link
		driver.get("https://www.dvidshub.net/");
		//login
		driver.findElement(By.cssSelector("a[href*='login']")).click();
		driver.findElement(By.cssSelector("[id='email']")).sendKeys(prop.getProperty("usernameadmin"));
		driver.findElement(By.cssSelector("[id ='password']")).sendKeys(prop.getProperty("passwordadmin"));
		driver.findElement(By.cssSelector("[id ='submit']")).click();
		
		return driver; 
	
	}
	

}
