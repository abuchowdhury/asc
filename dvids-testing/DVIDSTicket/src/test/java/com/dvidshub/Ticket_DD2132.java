
package com.dvidshub;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DriverCommand;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.openqa.selenium.JavascriptExecutor;

public class Ticket_DD2132 extends BaseForAdmin {

	// WebDriver driver;
	public ExtentHtmlReporter htmlReporter;
	public ExtentReports extent;
	public ExtentTest test;

	@BeforeTest
	public void setExtent() throws IOException {
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/myReport.html");
		htmlReporter.config().setDocumentTitle("Automation Report");
		htmlReporter.config().setReportName("Automation Report");
		htmlReporter.config().setTheme(Theme.STANDARD);

		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		// Passing General information

		extent.setSystemInfo("Tester Name", "Abu Chowdhury");
		driver = initializeDriver();

	}

	@AfterTest
	public void endReport() {
		extent.flush();
	}

	@Test(priority = 0)
	public void fullSizePhotoPreview() throws InterruptedException {

		test = extent.createTest("Ticket Number: DD-2132");

		//to pass authentication code and click on admin to navigate to cms
		driver.findElement(By.id("code")).sendKeys("619469");
		driver.findElement(By.id("submit")).click();
		driver.findElement(By.xpath("//*[@id=\"dvids_top_bar_left_2\"]/a")).click();
		
		//click on direct
		
		driver.findElement(By.id("menu-80")).click(); 
		
		//press try new direct button
		
		driver.findElement((By.xpath("//*[@id=\"modal-demo\"]/div/div/div[2]/form/div[2]/button[1]"))).click(); 
		
		//get text 'direct'
		String expecteddrectTextString =  driver.findElement(By.xpath("//*[@id=\"inner-page-navigation\"]/div/div[1]/span/span[1]")).getText(); 
		String actualString = "Direct"; 
		
		Assert.assertEquals(expecteddrectTextString, actualString); 
		System.out.println("Full Size Photo Preview-Checked"); 
		
		
	
		
	}

	
	

		

			
	
	
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); // to add name in extent report
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); // to add error/exception in extent
																					// report
			String screenshotPath = Ticket_DD2132.getScreenshot(driver, result.getName());
			test.addScreenCaptureFromPath(screenshotPath);// adding screen shot
		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
		}
		
		
	}

	// screenshot method
	public static String getScreenshot(WebDriver driver, String screenshotName) throws IOException {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);

		// after execution, you could see a folder "FailedTestsScreenshots" under src
		// folder
		String destination = System.getProperty("user.dir") + "/Screenshots/" + screenshotName + dateName + ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
	}
}
