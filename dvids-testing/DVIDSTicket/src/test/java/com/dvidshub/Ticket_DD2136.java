package com.dvidshub;

import static org.testng.Assert.ARRAY_MISMATCH_TEMPLATE;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class Ticket_DD2136 extends BaseForAdmin {

	// WebDriver driver;
	public ExtentHtmlReporter htmlReporter;
	public ExtentReports extent;
	public ExtentTest test;
	//List<WebElement> links = driver.findElements(By.tagName("img"));

	@BeforeTest
	public void setExtent() throws IOException {
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/myReport.html");
		htmlReporter.config().setDocumentTitle("Automation Report");
		htmlReporter.config().setReportName("Automation Report. Tested by - Abu Chowdhury");
		htmlReporter.config().setTheme(Theme.STANDARD);

		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		// Passing General information

		extent.setSystemInfo("Tester Name", "Abu Chowdhury");
		driver = initializeDriver();

	}

	@AfterTest
	public void endReport() {
		extent.flush();
	}
	/*
	 * @BeforeMethod public void setup() throws IOException { driver =
	 * initializeDriver(); }
	 * 
	 */

	@Test(priority = 0)
	public void mediaGalleryUpdate() throws InterruptedException {

		test = extent.createTest("DD-2136 - Media Gallery Update");
		
		

		//to pass authentication code and click on admin to navigate to cms
		driver.findElement(By.id("code")).sendKeys("477540");
		driver.findElement(By.id("submit")).click();
		
		
		
		driver.findElement(By.id("my_downloads_link")).click(); 
		driver.findElement(By.cssSelector("a[href*='mediagallery/detail/id/267207']")).click(); 
		driver.findElement(By.xpath("//*[@id=\"add_button-831652\"]/img")).click(); 
		
		//submit email
		driver.findElement(By.id("email")).sendKeys("llvllr.different@yahoo.com");
		driver.findElement(By.id("continue")).click();
		driver.findElement(By.xpath("//*[@id=\"loginstuff\"]/div/input")).click();
		
		String actualString = driver.findElement(By.xpath("/html/body/div[7]/div")).getText(); 
		//String expectedString = "Thank You for Your Request! An email has been sent to you with instructions on downloading the files you have requested. Thank you for choosing DVIDS."; 
		
		//Assert.assertEquals(actualString, expectedString); 
		
		if(actualString.contains("Thank You")) {
			System.out.println("Media Gallery Updated!"); 
		}
		else {
			System.out.println("Media Gallery not Updated!");
		}
		
		
	}

	
	

	
	
	
	
	
	
	public void brokenLinkCheck(ITestResult result) {

		String homePage = "https://www.dvidshub.net/";
		String url = "";
		HttpURLConnection huc = null;
		int respCode = 200;
		int linkCount = 0; 
		
		  List<WebElement> links = driver.findElements(By.tagName("img"));
		  
		  System.out.println("The number of  total link in   " + result.getName() +" is "+ links.size());
		 
		 
		Iterator<WebElement> it = links.iterator();

		while (it.hasNext()) {

			url = it.next().getAttribute("src");

			//System.out.println(url);

			if (url == null || url.isEmpty()) {
				System.out.println("URL is either not configured for anchor tag or it is empty");
				continue;
			}
			/*
			 * if (!url.startsWith(homePage)) {
			 * System.out.println("URL belongs to another domain, skipping it."); continue;
			 * }
			 */
			try {
				huc = (HttpURLConnection) (new URL(url).openConnection());

				huc.setRequestMethod("HEAD");

				huc.connect();

				respCode = huc.getResponseCode();

				if (respCode >= 400) {
					System.out.println(url + " is a broken link");
					linkCount++; 
				}
				
				/*
				else {
					System.out.println(url + " is a valid link");
				}
*/
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		System.out.println("total broken link : " + linkCount); 
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); // to add name in extent report
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); // to add error/exception in extent
																					// report
			String screenshotPath = Ticket_DD2136.getScreenshot(driver, result.getName());
			test.addScreenCaptureFromPath(screenshotPath);// adding screen shot
		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
		}

		brokenLinkCheck(result);

	}

	// screenshot method
	public static String getScreenshot(WebDriver driver, String screenshotName) throws IOException {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);

		// after execution, you could see a folder "FailedTestsScreenshots" under src
		// folder
		String destination = System.getProperty("user.dir") + "/Screenshots/" + screenshotName + dateName + ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
	}

}
