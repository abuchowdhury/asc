package com.dvidshub;

import static org.testng.Assert.ARRAY_MISMATCH_TEMPLATE;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class Ticket_DD2085 extends Base {

	// WebDriver driver;
	public ExtentHtmlReporter htmlReporter;
	public ExtentReports extent;
	public ExtentTest test;
	//List<WebElement> links = driver.findElements(By.tagName("img"));

	@BeforeTest
	public void setExtent() throws IOException {
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/myReport.html");
		htmlReporter.config().setDocumentTitle("Automation Report");
		htmlReporter.config().setReportName("Automation Report. Tested By - Abu Chowdhury");
		htmlReporter.config().setTheme(Theme.STANDARD);

		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		// Passing General information

		extent.setSystemInfo("Tester Name", "Abu Chowdhury");
		driver = initializeDriver();

	}

	@AfterTest
	public void endReport() {
		extent.flush();
	}
	/*
	 * @BeforeMethod public void setup() throws IOException { driver =
	 * initializeDriver(); }
	 * 
	 */

	@Test(priority = 0)
	public void navigateToPastAward() throws InterruptedException {

		test = extent.createTest("DD-2085 Awards: Winner page");
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(By.id("menuLinkstories"))).build().perform();
		driver.findElement(By.xpath("//*[@id=\"dvids_main_nav\"]/li[4]/ul/li[3]/a")).click();
		driver.findElement(By.cssSelector("a[href*='/awards/dma']")).click();
		

	}

	
	

	@Test(priority = 1)
	public void navigateTo2020() {
		
		System.out.println("Navigated to awards 2020 to pull out broken links"); 
		driver.findElement(By.cssSelector("a[href*='awards/dma/2020']")).click();
	
	}
	

	
	
	
	@Test(priority = 2)
	public void navigateTo2019() {
		
		System.out.println("Navigated to awards 2019 to pull out broken links"); 
		driver.findElement(By.cssSelector("a[href*='awards/dma/2019']")).click();
	
	}


	@Test(priority = 3)
	public void navigateTo2018() {
		
		System.out.println("----------Navigated to awards 2018 to pull out broken links-------"); 
		driver.findElement(By.cssSelector("a[href*='awards/dma/2018']")).click();
	
	}
	
	@Test(priority = 4)
	public void navigateTo2017() {
		
		System.out.println("----------Navigated to awards 2017 to pull out broken links-------"); 
		driver.findElement(By.cssSelector("a[href*='awards/dma/2017']")).click();
	
	}
	
	
	@Test(priority = 5)
	public void navigateTo2016() {
		
		System.out.println("----------Navigated to awards 2016 to pull out broken links-------"); 
		driver.findElement(By.cssSelector("a[href*='awards/dma/2016']")).click();
	
		
	}
	
	
	
	
	
	public void brokenLinkCheck(ITestResult result) {

		String homePage = "https://www.dvidshub.net/";
		String url = "";
		HttpURLConnection huc = null;
		int respCode = 200;
		int linkCount = 0; 
		
		  List<WebElement> links = driver.findElements(By.tagName("img"));
		  
		  System.out.println("The number of  total link in   " + result.getName() +" is "+ links.size());
		 
		 
		Iterator<WebElement> it = links.iterator();

		while (it.hasNext()) {

			url = it.next().getAttribute("src");

			//System.out.println(url);

			if (url == null || url.isEmpty()) {
				System.out.println("URL is either not configured for anchor tag or it is empty");
				continue;
			}
			/*
			 * if (!url.startsWith(homePage)) {
			 * System.out.println("URL belongs to another domain, skipping it."); continue;
			 * }
			 */
			try {
				huc = (HttpURLConnection) (new URL(url).openConnection());

				huc.setRequestMethod("HEAD");

				huc.connect();

				respCode = huc.getResponseCode();

				if (respCode >= 400) {
					System.out.println(url + " is a broken link");
					linkCount++; 
				}
				
				/*
				else {
					System.out.println(url + " is a valid link");
				}
*/
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		System.out.println("total broken link : " + linkCount); 
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); // to add name in extent report
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); // to add error/exception in extent
																					// report
			String screenshotPath = Ticket_DD2085.getScreenshot(driver, result.getName());
			test.addScreenCaptureFromPath(screenshotPath);// adding screen shot
		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
		}

		brokenLinkCheck(result);

	}

	// screenshot method
	public static String getScreenshot(WebDriver driver, String screenshotName) throws IOException {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);

		// after execution, you could see a folder "FailedTestsScreenshots" under src
		// folder
		String destination = System.getProperty("user.dir") + "/Screenshots/" + screenshotName + dateName + ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
	}

}
