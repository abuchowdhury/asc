
package com.dvidshub;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class Ticket_DD2139 extends BaseForAdmin {

	// WebDriver driver;
	public ExtentHtmlReporter htmlReporter;
	public ExtentReports extent;
	public ExtentTest test;

	@BeforeTest
	public void setExtent() throws IOException {
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/myReport.html");
		htmlReporter.config().setDocumentTitle("Automation Report");
		htmlReporter.config().setReportName("Automation Report. Tested By - Abu Chowdhury");
		htmlReporter.config().setTheme(Theme.STANDARD);

		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		// Passing General information

		extent.setSystemInfo("Tester Name", "Abu Chowdhury");
		driver = initializeDriver();

	}

	@AfterTest
	public void endReport() {
		extent.flush();
	}
	

	@Test(priority = 0)
	public void mediaRequestEmail() {
		
		test = extent.createTest("DD-2139 ESR/Media Request email");
		//dual factor authentication code
		driver.findElement(By.id("code")).sendKeys("471280");
		driver.findElement(By.id("submit")).click();
		driver.findElement(By.xpath("//*[@id=\"dvids_top_bar_left_2\"]/a")).click();
		
		//click on event support
		driver.findElement(By.id("menu-98")).click();  
		driver.findElement(By.cssSelector("a[href*='satellitetruck/list/type/Media']")).click();
		driver.findElement(By.cssSelector("a[href*='/satellitetruck/list/approved/Pending']")).click(); 
		
		//24702
		String approvalStatus = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/div/div[3]/table/tbody/tr[7]/td[11]")).getText(); 
		String status = driver.findElement(By.xpath("//*[@id=\"content\"]/div/div/div[3]/table/tbody/tr[7]/td[10]")).getText(); 
		
		
		//24644
		String approvalStatus24644 = driver.findElement(By.xpath("//*[@id=\"content\"]/div/div/div[3]/table/tbody/tr[12]/td[11]")).getText(); 
		String status24644 = driver.findElement(By.xpath("//*[@id=\"content\"]/div/div/div[3]/table/tbody/tr[12]/td[10]")).getText(); 
		
		System.out.println("Id: 24702 " + "Status: " + status + ", " + "Approved: " + approvalStatus); 
		System.out.println("Id: 24644 " + "Status: " + status24644 + ", " + "Approved: " + approvalStatus24644); 
		
		
		
	}
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); // to add name in extent report
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); // to add error/exception in extent
																					// report
			String screenshotPath = Ticket_DD2139.getScreenshot(driver, result.getName());
			test.addScreenCaptureFromPath(screenshotPath);// adding screen shot
		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
		}

	}

	// screenshot method
	public static String getScreenshot(WebDriver driver, String screenshotName) throws IOException {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);

		// after execution, you could see a folder "FailedTestsScreenshots" under src
		// folder
		String destination = System.getProperty("user.dir") + "/Screenshots/" + screenshotName + dateName + ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
	}
}
