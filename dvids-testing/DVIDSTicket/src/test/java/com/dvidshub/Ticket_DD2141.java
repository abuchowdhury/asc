
package com.dvidshub;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class Ticket_DD2141 extends BaseForAdmin {

	// WebDriver driver;
	public ExtentHtmlReporter htmlReporter;
	public ExtentReports extent;
	public ExtentTest test;

	@BeforeTest
	public void setExtent() throws IOException {
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/myReport.html");
		htmlReporter.config().setDocumentTitle("Automation Report");
		htmlReporter.config().setReportName("Automation Report. Tested By - Abu Chowdhury");
		htmlReporter.config().setTheme(Theme.STANDARD);

		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		// Passing General information

		extent.setSystemInfo("Tester Name", "Abu Chowdhury");
		driver = initializeDriver();

	}

	@AfterTest
	public void endReport() {
		extent.flush();
	}
	

	@Test(priority = 0)
	public void unsubscribeFromThisGroup() {
		
		test = extent.createTest("DD-2141 We no longer need these emails");
		//dual factor authentication code
		driver.findElement(By.id("code")).sendKeys("249677");
		driver.findElement(By.id("submit")).click();
		driver.findElement(By.xpath("//*[@id=\"dvids_top_bar_left_2\"]/a")).click();
		
		driver.navigate().to("http://email.mailgun.dvidshub.net/u/eJxljdsKwyAQRL-mvhm8bWIe_JawusYIiSmN6fdX-1YKw8DA4Qy9M108Hph33iuXxPczLZmcnUCPUktGzlhg2SmhWuQkZ6OEHrRH6EMagetK8WFEN6S7DNSt2-2HEivbnBEajBGBRrQamhc8AYKSs5gNCMu-PA94PDGnwts55VcMdfFYwxb284rs5RoUz_byY6__6Af2fURG");
		driver.findElement(By.id("submit")).click(); 
		String unsubscribeMsg  = driver.findElement(By.xpath("/html/body/div/div/p")).getText(); 
		String expected = "You have unsubscribed. Thanks!"; 	
		Assert.assertEquals(expected, unsubscribeMsg); 
		System.out.println("Unsubscribed From This Group");
		
	
		
	}
	
	@Test(priority = 1)
	public void unsubscribeFromAlldvidshubnet() {
		
		
		driver.navigate().to("http://email.mailgun.dvidshub.net/u/eJxVjUsOhCAQRE8jO0zzU1ywmJMYmkYk8TNRnPMPzG6SSieVvH5Fn0w3j7vPG28nH4lvZ5ozOTsaNQglGDltDctOgqwRo5i0BNUr9KYVocEvC8VOQzOk5-ipWdcH-yMWtjoYFKKsKBiLMAYaJhSxvnlvLSpgP54Hv799Tgev45SvGMqMvoQ1bOcd2eUqFM-68mcvrpOvL5-YPso");
		driver.findElement(By.id("submit")).click(); 
		String unsubscribeMsgVideo  = driver.findElement(By.xpath("/html/body/div/div/p")).getText(); 
		String expectedForVideo = "You have unsubscribed. Thanks!"; 	
		Assert.assertEquals(expectedForVideo, unsubscribeMsgVideo); 
		System.out.println("Unsubscribed from all dvidshub.net");
	}
	

	
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); // to add name in extent report
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); // to add error/exception in extent
																					// report
			String screenshotPath = Ticket_DD2141.getScreenshot(driver, result.getName());
			test.addScreenCaptureFromPath(screenshotPath);// adding screen shot
		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
		}

	}

	// screenshot method
	public static String getScreenshot(WebDriver driver, String screenshotName) throws IOException {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);

		// after execution, you could see a folder "FailedTestsScreenshots" under src
		// folder
		String destination = System.getProperty("user.dir") + "/Screenshots/" + screenshotName + dateName + ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
	}
}
