
package com.dvidshub;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.openqa.selenium.JavascriptExecutor;

public class Ticket_DD2109 extends Base {

	// WebDriver driver;
	public ExtentHtmlReporter htmlReporter;
	public ExtentReports extent;
	public ExtentTest test;

	@BeforeTest
	public void setExtent() throws IOException {
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/myReport.html");
		htmlReporter.config().setDocumentTitle("Automation Report");
		htmlReporter.config().setReportName("Automation Report. Tested By - Abu Chowdhury");
		htmlReporter.config().setTheme(Theme.STANDARD);

		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		// Passing General information

		extent.setSystemInfo("Tester Name", "Abu Chowdhury");
		driver = initializeDriver();

	}

	@AfterTest
	public void endReport() {
		extent.flush();
	}

	@Test(priority = 0)
	public void publicationCheck() throws InterruptedException {

		test = extent.createTest("DD-2109 - Publication Query before 1970");

		driver.navigate().to("https://www.dvidshub.net/publication/447/newscastle");
		driver.findElement(By.id("issueYearDropDown")).click();
		driver.findElement(By.xpath("//*[@id=\"issueYearDropDown\"]/option[39]")).click();

		String publicationDateFor1968 = driver.findElement(By.xpath("//*[@id=\"dpdc\"]/a")).getText();
		String expectedString1968 = "06.14.1968";

		Assert.assertEquals(expectedString1968, publicationDateFor1968);

		System.out.println("Data loaded from 1968");
		System.out.println("Publication date is: " + publicationDateFor1968);

		// to check data for 1969
		driver.findElement(By.id("issueYearDropDown")).click();
		driver.findElement(By.xpath("//*[@id=\"issueYearDropDown\"]/option[38]")).click();

		String publicationDateFor1969 = driver.findElement(By.xpath("//*[@id=\"dpdc\"]/a")).getText();
		String expectedString1969 = "01.15.1969";

		Assert.assertEquals(expectedString1969, publicationDateFor1969);

		System.out.println("Data loaded from 1969");
		System.out.println("Publication date is: " + publicationDateFor1969);

		// to check data for 1970
		driver.findElement(By.id("issueYearDropDown")).click();
		driver.findElement(By.xpath("//*//*[@id=\"issueYearDropDown\"]/option[37]")).click();

		String publicationDateFor1970 = driver.findElement(By.xpath("//*[@id=\"dpdc\"]/a")).getText();
		String expectedString1970 = "02.11.1970";

		Assert.assertEquals(expectedString1970, publicationDateFor1970);

		System.out.println("Data loaded from 1970");
		System.out.println("Publication date is: " + publicationDateFor1970);

		Thread.sleep(5000); 
		 //driver.quit();
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); // to add name in extent report
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); // to add error/exception in extent
																					// report
			String screenshotPath = Ticket_DD2109.getScreenshot(driver, result.getName());
			test.addScreenCaptureFromPath(screenshotPath);// adding screen shot
		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
		}

	}

	// screenshot method
	public static String getScreenshot(WebDriver driver, String screenshotName) throws IOException {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);

		// after execution, you could see a folder "FailedTestsScreenshots" under src
		// folder
		String destination = System.getProperty("user.dir") + "/Screenshots/" + screenshotName + dateName + ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
	}
}
