
package com.dvidshub;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class ListViewBrokenImageLink extends Base {

	// private static WebDriver driver = null;

	@Test(priority = 0)
	public void initializeConnection() throws IOException {

		driver = initializeDriver();
		//driver.get("https://www.dvidshub.net/");
		//driver.manage().window().maximize();
/*
		// login
		driver.findElement(By.cssSelector("a[href*='login']")).click();
		driver.findElement(By.cssSelector("[id='email']")).sendKeys(prop.getProperty("username"));
		driver.findElement(By.cssSelector("[id = 'password']")).sendKeys(prop.getProperty("password"));
		driver.findElement(By.cssSelector("[id = 'submit']")).click();
	*/	
		driver.navigate().to("https://www.dvidshub.net/search/?q=&filter%5Btype%5D=video&view=list&sort=publishdate&page=30010");
		
		
		

	}

	@Test(invocationCount = 200)
	public void linkChecker() {

		// String totalPageNum =
		// driver.findElement(By.xpath("//*[@id=\"body_content\"]/div/div/div/ul/li[8]/a")).getText();

		String homePage = "https://www.dvidshub.net/";
		String url = "";
		HttpURLConnection huc = null;
		int respCode = 200;

		List<WebElement> links = driver.findElements(By.tagName("img"));

		Iterator<WebElement> it = links.iterator();

		while (it.hasNext()) {

			url = it.next().getAttribute("src");

			//System.out.println(url);

			if (url == null || url.isEmpty()) {
				System.out.println("URL is either not configured for anchor tag or it is empty");
				continue;
			}

			try {
				huc = (HttpURLConnection) (new URL(url).openConnection());

				huc.setRequestMethod("HEAD");

				huc.connect();

				respCode = huc.getResponseCode();

				if (respCode >= 400) {
					System.out.println(url + " image link is broken");
				} 

			} catch (MalformedURLException e) {
// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		driver.findElement(By.cssSelector("a[class=\'pager_next']")).click();

	}

}
