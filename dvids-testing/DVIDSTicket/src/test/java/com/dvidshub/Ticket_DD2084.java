package com.dvidshub;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;



public class Ticket_DD2084 extends Base {

	// WebDriver driver;
	public ExtentHtmlReporter htmlReporter;
	public ExtentReports extent;
	public ExtentTest test;

	@BeforeTest
	public void setExtent() {
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/myReport.html");
		htmlReporter.config().setDocumentTitle("Automation Report");
		htmlReporter.config().setReportName("Automation Report. Tested by - Abu Chowdhury");
		htmlReporter.config().setTheme(Theme.STANDARD);

		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		// Passing General information

		extent.setSystemInfo("Tester Name", "Abu Chowdhury");

	}

	@AfterTest
	public void endReport() {
		extent.flush();
	}

	@BeforeMethod
	public void setup() throws IOException {
		driver = initializeDriver();
	}

	@Test
	public void downloadGalleryLink1() {

		test = extent.createTest("DD-2084 Download Gallery");

		// navigate to desired url
		driver.navigate().to("https://www.dvidshub.net/image/4518821/usaf-thunderbirds-arrived-june-20-hil");

		// download single photo
		driver.findElement(By.cssSelector("a[href*='/download/image']")).click();
		test.createNode("Photo Downloaded Successfully"); 
		System.out.println("Photo Downloaded successfully");
		// download gallery
		driver.findElement(By.linkText("Download Gallery")).click();
		System.out.println("Gallery Downloaded Successfully");
		test.createNode("Gallery Downloaded Successfully"); 
		System.out.println("Test 1 passed");

	}

	@Test
	public void downloadGalleryLink2() {
	
	
		test = extent.createTest("DD-2084 Download Gallery Link 2");

		// navigate to desired url
		driver.navigate().to("https://www.dvidshub.net/image/5534329/alpha-warrior-athletes-compete-hill");

		// download single photo
		driver.findElement(By.cssSelector("a[href*='/download/image']")).click();
		test.createNode("Photo Downloaded Successfully"); 
		System.out.println("Photo Downloaded successfully");
		// download gallery
		driver.findElement(By.linkText("Download Gallery")).click();
		System.out.println("Gallery Downloaded Successfully");
		test.createNode("Gallery Downloaded Successfully"); 
		System.out.println("Test 1 passed");
		
		
		
		
	}
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); // to add name in extent report
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); // to add error/exception in extent
																					// report
			String screenshotPath = Ticket_DD2084.getScreenshot(driver, result.getName());
			test.addScreenCaptureFromPath(screenshotPath);// adding screen shot
		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
		}

	}

	// screenshot method
	public static String getScreenshot(WebDriver driver, String screenshotName) throws IOException {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);

		// after execution, you could see a folder "FailedTestsScreenshots" under src
		// folder
		String destination = System.getProperty("user.dir") + "/Screenshots/" + screenshotName + dateName + ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
	}
}
